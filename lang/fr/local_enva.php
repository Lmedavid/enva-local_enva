<?php
/**
 * @package   local_enva
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname']   = 'ENVA Plugin local';

$string['envaexternalrole_name'] = 'ENVA Rôle Externe';
$string['envaexternalrole_desc'] = 'ENVA Rôle Externe, utilisé principalement pour décider si un utilisateur doit passer
un examen avant de démarrer les cours';

// Settings string
$string['envasettings'] = 'Paramètres ENVA';
$string['setting:coursetocomplete'] = 'Cours à compléter';
$string['setting:coursetocomplete_desc'] = 'Cours à compléter à priori, de manière à être inscrit tous les cours
destinés au personnes externes';


// CRON

$string['registerusersintest'] = 'Enregistre les utilisateurs externes dans le cours de pré-test';

// ENVA Score page
$string['envascores'] = 'Scores for ENVA';