<?php
/**
 * @package   local_enva
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname']   = 'ENVA Local Plugin';

$string['envaexternalrole_name'] = 'ENVA External Role';
$string['envaexternalrole_desc'] = 'ENVA External Role, used mainly to decide if a user has to pass the main quiz
before starting';

// Settings string
$string['envasettings'] = 'ENVA Settings';
$string['setting:coursetocomplete'] = 'Course to complete';
$string['setting:coursetocomplete_desc'] = 'Course to complete before, in order to be registered to all external courses';


// CRON

$string['registerusersintest'] = 'Register users in test course';

// ENVA Score page
$string['envascores'] = 'Scores for ENVA';