<?php
/**
 * @package   local_enva
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;
global $CFG;

define('ENVA_EXTERNAL_ROLE_SHORTNAME','ENVA-EXT');
define('ENVA_EXTERNAL_ROLE_ARCHETYPE','student');
define('ENVA_EXTERNAL_COURSE_TAG_NAME','external_course');